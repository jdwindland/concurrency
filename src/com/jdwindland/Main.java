package com.jdwindland;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

public class Main {

    public static void main(String[] args) {

        //Manually run Thread. Thread is instantiated, started and then stopped.
        SampleThread st1 = new SampleThread(1);
        SampleThread st2 = new SampleThread(2);
        st1.start();
        st2.start();
        try{
            Thread.sleep(500);
            st1.stopThread();
            st2.stopThread();
            Thread.sleep(500);
        }catch(InterruptedException e){
            System.out.println("Thread was interrupted.");
        }catch(IllegalArgumentException e){
            System.out.println("The millis needs to be positive.");
        }

        //Manually run Runnable. Runnable is instantiated, started and then stopped.
        SampleRunnable sr1 = new SampleRunnable(1);
        SampleRunnable sr2 = new SampleRunnable(2);
        Thread t1 = new Thread(sr1);
        Thread t2 = new Thread(sr2);
        t1.start();
        t2.start();
        try{
            Thread.sleep(500);
            sr1.stopRunnable();
            sr2.stopRunnable();
            Thread.sleep(500);
        }catch(InterruptedException e){
            System.out.println("Thread was interrupted.");
        }catch(IllegalArgumentException e){
            System.out.println("The millis needs to be positive.");
        }

        //Executor used to start and shutdown (close all threads) Runnable. Runnable instantiated, executed, and then shutdown.
        ExecutorService executor = Executors.newFixedThreadPool(3);

        SampleExecutor se1 = new SampleExecutor("A");
        SampleExecutor se2 = new SampleExecutor("B");
        SampleExecutor se3 = new SampleExecutor("C");
        SampleExecutor se4 = new SampleExecutor("D");
        SampleExecutor se5 = new SampleExecutor("E");

        try{
            executor.execute(se1);
            executor.execute(se2);
            executor.execute(se3);
            executor.execute(se4);
            executor.execute(se5);
        }catch(RejectedExecutionException e){
            System.out.println("The task cannot be executed.");
        }catch(NullPointerException e){
            System.out.println("The command cannot be NULL.");
        }

        try{
            executor.shutdown();
        }catch(SecurityException e){
            System.out.println("Access denied. Please check your permissions.");
        }

    }

}