package com.jdwindland;

public class SampleExecutor implements Runnable{

    //Variable will be used to identify each thread.
    private String name;

    public SampleExecutor(String n){
        this.name = n;
    }

    //Method defines what the runnable will do. It will show the thread name and count up to 25 and then close the thread.
    public void run(){
        System.out.println("\n\nStarting thread " + name + ".\n");
        for (int i = 1; i < 26; i++){
            System.out.print("Thread " + name + " " + i + "\n");
        }
        System.out.println("\n\nThread " + name + " has ended.\n");
    }

}
