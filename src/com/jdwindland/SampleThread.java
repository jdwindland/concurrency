package com.jdwindland;

import java.util.concurrent.atomic.AtomicBoolean;

public class SampleThread extends Thread{

    //Atomic boolean variable will be used to determine if thread is running or stopped.
    private final AtomicBoolean running = new AtomicBoolean(false);
    int num;

    public SampleThread(int n){
        this.num = n;
    }

    //Run method sets Atomic boolean variable to true and defines what the thread will do.
    public void run(){
        running.set(true);
        System.out.println("\nSample thread " + num + " has started.\n");
    }

    //stopThread method sets Atomic boolean variable to false and stops the thread.
    public void stopThread(){
        running.set(false);
        System.out.println("\nSample thread " + num + " has stopped.\n");
    }

}
