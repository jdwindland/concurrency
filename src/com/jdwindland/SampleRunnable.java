package com.jdwindland;

import java.util.concurrent.atomic.AtomicBoolean;

public class SampleRunnable implements Runnable {

    //Atomic boolean variable will be used to determine if runnable is running or stopped.
    private final AtomicBoolean running = new AtomicBoolean(false);
    int num;

    public SampleRunnable (int n){
        this.num = n;
    }

    //Run method sets Atomic boolean variable to true and defines what the runnable will do.
    @Override
    public void run() {
        running.set(true);
        System.out.println("\nSample runnable " + num + " has started.\n");
    }

    //stopRunnable method sets Atomic boolean variable to false and stops the runnable.
    public void stopRunnable(){
        running.set(false);
        System.out.println("\nSample runnable " + num + " has stopped.\n");
    }

}
